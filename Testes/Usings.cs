global using Microsoft.VisualStudio.TestTools.UnitTesting;
global using Microsoft.Extensions.DependencyInjection;
global using controle_tarefas.Model;
global using controle_tarefas.Enums;
global using controle_tarefas.Intefaces;
global using controle_tarefas.Repositorio;
global using controle_tarefas.Negocio;