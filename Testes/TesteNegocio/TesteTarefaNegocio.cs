namespace Testes
{

    [TestClass]
    public class TesteTarefaNegocio
    {
        private ITarefaNegocio _tarefaNegocio;
        private readonly Moq.Mock<IUsuarioRepositorio> _mockUsuarioRepositorio;
        private readonly Moq.Mock<IUsuarioNegocio> _mockUsuarioNegocio;
        private readonly Moq.Mock<ITarefaRepositorio> _mockTarefaRepositorio;

        public TesteTarefaNegocio()
        {
            _mockUsuarioRepositorio = new Moq.Mock<IUsuarioRepositorio>();
            _mockUsuarioNegocio = new Moq.Mock<IUsuarioNegocio>();
            _mockTarefaRepositorio = new Moq.Mock<ITarefaRepositorio>();
        }

        [TestMethod]
        public void TesteVisualizarMinhasTarefas()
        {
            int usuarioId = 1;
            List<Tarefa> tarefas = new List<Tarefa>();
            tarefas.Add(new Tarefa { Id = 2, Nome = "AdicionarFeature", Descricao = "Adicionar feature visualizar tarefas", usuarioResponsavel = "Pedro", IdUsuario = 1, Status = TarefaStatus.EmAndamento });

            _mockUsuarioRepositorio.Setup(UsuarioRepositorio => UsuarioRepositorio.BuscarIdUsuarioAtual()).Returns(usuarioId);
            _mockTarefaRepositorio.Setup(TarefaRepositorio => TarefaRepositorio.BuscarTarefasPeloResponsavel(usuarioId)).Returns(tarefas);
            _tarefaNegocio = new TarefaNegocio(_mockUsuarioNegocio.Object,
                                                 _mockUsuarioRepositorio.Object,
                                                _mockTarefaRepositorio.Object);
            try
            {
                _tarefaNegocio.VisualizarMinhasTarefas();
            }
            finally
            {
                Assert.IsTrue(true);
            }
        }
        [TestMethod]
        public void TesteVisualizarMinhasTarefasListaVazia()
        {
            int usuarioId = 1;
            List<Tarefa> tarefas = new List<Tarefa>();

            _mockUsuarioRepositorio.Setup(UsuarioRepositorio => UsuarioRepositorio.BuscarIdUsuarioAtual()).Returns(usuarioId);
            _mockTarefaRepositorio.Setup(TarefaRepositorio => TarefaRepositorio.BuscarTarefasPeloResponsavel(usuarioId)).Returns(tarefas);
            _tarefaNegocio = new TarefaNegocio(_mockUsuarioNegocio.Object,
                                                 _mockUsuarioRepositorio.Object,
                                                _mockTarefaRepositorio.Object);

            var ex = Assert.ThrowsException<Exception>(() => _tarefaNegocio.VisualizarMinhasTarefas());
            Assert.AreEqual("Nao ha tarefas a serem mostradas", ex.Message);
        }
        [TestMethod]
        public void TesteVisualizarTarefasOutroUsuario()
        {
            var usuario = new Usuario() { Id = 1, Nome = "Joao" };
            List<Tarefa> tarefas = new List<Tarefa>();
            tarefas.Add(new Tarefa { Id = 2, Nome = "AdicionarFeature", Descricao = "Adicionar feature visualizar tarefas", usuarioResponsavel = "Joao", IdUsuario = 1, Status = TarefaStatus.EmAndamento });

            _mockTarefaRepositorio.Setup(TarefaRepositorio => TarefaRepositorio.BuscarTarefasPeloResponsavel(usuario.Id)).Returns(tarefas);
            _mockUsuarioRepositorio.Setup(UsuarioRepositorio => UsuarioRepositorio.BuscarUsuarioPorId(usuario.Id)).Returns(usuario);
            _tarefaNegocio = new TarefaNegocio(_mockUsuarioNegocio.Object,
                                                 _mockUsuarioRepositorio.Object,
                                                _mockTarefaRepositorio.Object);
            try
            {
                _tarefaNegocio.VisualizarTarefasUsuario(usuario.Id);
            }
            finally
            {
                Assert.IsTrue(true);
            }
        }
        [TestMethod]
        public void TesteVisualizarTarefasOutroUsuarioQueNaoPossuiTarefasAtribuidas()
        {
            var usuario = new Usuario() { Id = 1, Nome = "Joao" };
            List<Tarefa> tarefas = new List<Tarefa>();

            _mockTarefaRepositorio.Setup(TarefaRepositorio => TarefaRepositorio.BuscarTarefasPeloResponsavel(usuario.Id)).Returns(tarefas);
            _mockUsuarioRepositorio.Setup(UsuarioRepositorio => UsuarioRepositorio.BuscarUsuarioPorId(usuario.Id)).Returns(usuario);
            _tarefaNegocio = new TarefaNegocio(_mockUsuarioNegocio.Object,
                                                 _mockUsuarioRepositorio.Object,
                                                _mockTarefaRepositorio.Object);
            var ex = Assert.ThrowsException<Exception>(() => _tarefaNegocio.VisualizarTarefasUsuario(usuario.Id));
            Assert.AreEqual("Nao ha tarefas a serem mostradas", ex.Message);
        }
        [TestMethod]
        public void TesteMudarResponsavelPelaTarefaUsuarioJaCadastrado()
        {
            var usuario = new Usuario() { Id = 1, Nome = "Joao" };

            var tarefa = new Tarefa() { Id = 2, Nome = "AdicionarFeature", Descricao = "Adicionar feature visualizar tarefas", usuarioResponsavel = "Pedro", IdUsuario = 2, Status = TarefaStatus.EmAndamento };

            _mockTarefaRepositorio.Setup(TarefaRepositorio => TarefaRepositorio.BuscarTarefaPeloId(tarefa.Id)).Returns(tarefa);
            _mockUsuarioRepositorio.Setup(UsuarioRepositorio => UsuarioRepositorio.BuscarUsuarioPorId(usuario.Id)).Returns(usuario);
            _tarefaNegocio = new TarefaNegocio(_mockUsuarioNegocio.Object,
                                                 _mockUsuarioRepositorio.Object,
                                                _mockTarefaRepositorio.Object);
            try
            {
                _tarefaNegocio.MudarResponsavelPelaTarefa(tarefa.Id, usuario);
            }
            finally
            {
                Assert.IsTrue(true);
            }
        }
        [TestMethod]
        public void TesteMudarResponsavelPelaTarefaUsuarioNaoCadastrado()
        {
            var usuario = new Usuario() { Id = 1, Nome = "Joao" };

            var tarefa = new Tarefa() { Id = 2, Nome = "AdicionarFeature", Descricao = "Adicionar feature visualizar tarefas", usuarioResponsavel = "Pedro", IdUsuario = 2, Status = TarefaStatus.EmAndamento };

            _mockTarefaRepositorio.Setup(TarefaRepositorio => TarefaRepositorio.BuscarTarefaPeloId(tarefa.Id)).Returns(tarefa);
            _mockUsuarioRepositorio.Setup(UsuarioRepositorio => UsuarioRepositorio.BuscarUsuarioPorId(usuario.Id)).Returns((Usuario)null);
            _mockUsuarioNegocio.Setup(UsuarioNegocio => UsuarioNegocio.CadastrarUsuario(usuario));
            _tarefaNegocio = new TarefaNegocio(_mockUsuarioNegocio.Object,
                                                 _mockUsuarioRepositorio.Object,
                                                _mockTarefaRepositorio.Object);
            try
            {
                _tarefaNegocio.MudarResponsavelPelaTarefa(tarefa.Id, usuario);
            }
            finally
            {
                Assert.IsTrue(true);
            }
        }
        [TestMethod]
        public void TesteMudarResponsavelPelaTarefaEmQueTarefaNaoExiste()
        {
            var usuario = new Usuario() { Id = 1, Nome = "Joao" };

            var tarefaId = 5;

            _mockTarefaRepositorio.Setup(TarefaRepositorio => TarefaRepositorio.BuscarTarefaPeloId(tarefaId)).Returns((Tarefa)null);
            _tarefaNegocio = new TarefaNegocio(_mockUsuarioNegocio.Object,
                                                 _mockUsuarioRepositorio.Object,
                                                _mockTarefaRepositorio.Object);
            var ex = Assert.ThrowsException<Exception>(() => _tarefaNegocio.MudarResponsavelPelaTarefa(tarefaId, usuario));
            Assert.AreEqual("Tarefa nao existe", ex.Message);
        }
        [TestMethod]
        public void TesteCadastrarTarefa()
        {
            var usuario = new Usuario() { Id = 2, Nome = "Joao" };

            var tarefa = new Tarefa() { Id = 2, Nome = "AdicionarFeature", Descricao = "Adicionar feature visualizar tarefas", usuarioResponsavel = "Joao", IdUsuario = 2, Status = TarefaStatus.EmAndamento };

            _mockTarefaRepositorio.Setup(TarefaRepositorio => TarefaRepositorio.BuscarTarefaPeloId(tarefa.Id)).Returns((Tarefa)null);
            _mockUsuarioRepositorio.Setup(UsuarioRepositorio => UsuarioRepositorio.BuscarUsuarioPorId(tarefa.IdUsuario)).Returns(usuario);
            _tarefaNegocio = new TarefaNegocio(_mockUsuarioNegocio.Object,
                                                 _mockUsuarioRepositorio.Object,
                                                _mockTarefaRepositorio.Object);
            try
            {
                _tarefaNegocio.CadastrarTarefa(tarefa);
            }
            finally
            {
                Assert.IsTrue(true);
            }
        }
        [TestMethod]
        public void TesteCadastrarTarefaNomeInvalido()
        {

            var tarefa = new Tarefa() { Id = 2, Descricao = "Adicionar feature visualizar tarefas", usuarioResponsavel = "Pedro", IdUsuario = 2, Status = TarefaStatus.EmAndamento };

            _tarefaNegocio = new TarefaNegocio(_mockUsuarioNegocio.Object,
                                                 _mockUsuarioRepositorio.Object,
                                                _mockTarefaRepositorio.Object);

            var ex = Assert.ThrowsException<Exception>(() => _tarefaNegocio.CadastrarTarefa(tarefa));
            Assert.AreEqual("Nome inválido", ex.Message);
        }
        [TestMethod]
        public void TesteCadastrarTarefaIdInvalido()
        {

            var tarefa = new Tarefa() { Nome = "AdicionarFeature", Descricao = "Adicionar feature visualizar tarefas", usuarioResponsavel = "Pedro", IdUsuario = 2, Status = TarefaStatus.EmAndamento };


            _tarefaNegocio = new TarefaNegocio(_mockUsuarioNegocio.Object,
                                                 _mockUsuarioRepositorio.Object,
                                                _mockTarefaRepositorio.Object);

            var ex = Assert.ThrowsException<Exception>(() => _tarefaNegocio.CadastrarTarefa(tarefa));
            Assert.AreEqual("Id invalido", ex.Message);
        }
        [TestMethod]
        public void TesteCadastrarTarefaEmQueIdJaExiste()
        {

            var tarefa = new Tarefa() { Id = 2, Nome = "AdicionarFeature", Descricao = "Adicionar feature visualizar tarefas", usuarioResponsavel = "Pedro", IdUsuario = 2, Status = TarefaStatus.EmAndamento };

            _mockTarefaRepositorio.Setup(TarefaRepositorio => TarefaRepositorio.BuscarTarefaPeloId(tarefa.Id)).Returns(tarefa);

            _tarefaNegocio = new TarefaNegocio(_mockUsuarioNegocio.Object,
                                                 _mockUsuarioRepositorio.Object,
                                                _mockTarefaRepositorio.Object);

            var ex = Assert.ThrowsException<Exception>(() => _tarefaNegocio.CadastrarTarefa(tarefa));
            Assert.AreEqual("Tarefa ja existe", ex.Message);
        }
        [TestMethod]
        public void TesteCadastrarTarefaDescricaoInvalida()
        {

            var tarefa = new Tarefa() { Id = 2, Nome = "AdicionarFeature", usuarioResponsavel = "Pedro", IdUsuario = 2, Status = TarefaStatus.EmAndamento };

            _mockTarefaRepositorio.Setup(TarefaRepositorio => TarefaRepositorio.BuscarTarefaPeloId(tarefa.Id)).Returns((Tarefa)null);

            _tarefaNegocio = new TarefaNegocio(_mockUsuarioNegocio.Object,
                                                 _mockUsuarioRepositorio.Object,
                                                _mockTarefaRepositorio.Object);

            var ex = Assert.ThrowsException<Exception>(() => _tarefaNegocio.CadastrarTarefa(tarefa));
            Assert.AreEqual("Descricao invalida", ex.Message);
        }
        [TestMethod]
        public void TesteCadastrarTarefaUsuarioIdNaoExiste()
        {

            var tarefa = new Tarefa() { Id = 2, Nome = "AdicionarFeature", Descricao = "Adicionar feature visualizar tarefas", usuarioResponsavel = "Pedro", IdUsuario = 2, Status = TarefaStatus.EmAndamento };

            _mockTarefaRepositorio.Setup(TarefaRepositorio => TarefaRepositorio.BuscarTarefaPeloId(tarefa.Id)).Returns((Tarefa)null);
            _mockUsuarioRepositorio.Setup(UsuarioRepositorio => UsuarioRepositorio.BuscarUsuarioPorId(tarefa.IdUsuario)).Returns((Usuario)null);
            _tarefaNegocio = new TarefaNegocio(_mockUsuarioNegocio.Object,
                                                 _mockUsuarioRepositorio.Object,
                                                _mockTarefaRepositorio.Object);

            var ex = Assert.ThrowsException<Exception>(() => _tarefaNegocio.CadastrarTarefa(tarefa));
            Assert.AreEqual("Usuario responsavel nao cadastrado", ex.Message);
        }
        [TestMethod]
        public void TesteConcluirTarefa()
        {

            var tarefa = new Tarefa() { Id = 2, Nome = "AdicionarFeature", Descricao = "Adicionar feature visualizar tarefas", usuarioResponsavel = "Pedro", IdUsuario = 2, Status = TarefaStatus.EmAndamento };

            _mockTarefaRepositorio.Setup(TarefaRepositorio => TarefaRepositorio.BuscarTarefaPeloId(tarefa.Id)).Returns(tarefa);
            _mockTarefaRepositorio.Setup(TarefaRepositorio => TarefaRepositorio.ConcluirTarefa(tarefa.Id));
            _tarefaNegocio = new TarefaNegocio(_mockUsuarioNegocio.Object,
                                                 _mockUsuarioRepositorio.Object,
                                                _mockTarefaRepositorio.Object);

            try
            {
                _tarefaNegocio.ConcluirTarefa(tarefa.Id);
            }
            finally
            {
                Assert.IsTrue(true);
            }
        }
        [TestMethod]
        public void TesteConcluirTarefaQueNaoExiste()
        {

            var tarefaId = 1;

            _mockTarefaRepositorio.Setup(TarefaRepositorio => TarefaRepositorio.BuscarTarefaPeloId(tarefaId)).Returns((Tarefa)null);

            _tarefaNegocio = new TarefaNegocio(_mockUsuarioNegocio.Object,
                                                 _mockUsuarioRepositorio.Object,
                                                _mockTarefaRepositorio.Object);

            var ex = Assert.ThrowsException<Exception>(() => _tarefaNegocio.ConcluirTarefa(tarefaId));
            Assert.AreEqual("Tarefa nao existe", ex.Message);
        }
        [TestMethod]
        public void TesteConcluirTarefaStatusInvalido()
        {

            var tarefa = new Tarefa() { Id = 2, Nome = "AdicionarFeature", Descricao = "Adicionar feature visualizar tarefas", usuarioResponsavel = "Pedro", IdUsuario = 2, Status = TarefaStatus.Cancelada };

            _mockTarefaRepositorio.Setup(TarefaRepositorio => TarefaRepositorio.BuscarTarefaPeloId(tarefa.Id)).Returns(tarefa);

            _tarefaNegocio = new TarefaNegocio(_mockUsuarioNegocio.Object,
                                                 _mockUsuarioRepositorio.Object,
                                                _mockTarefaRepositorio.Object);

            var ex = Assert.ThrowsException<Exception>(() => _tarefaNegocio.ConcluirTarefa(tarefa.Id));
            Assert.AreEqual("Tarefa ja foi cancelada ou concluída", ex.Message);
        }
        [TestMethod]
        public void TesteCancelarTarefa()
        {

            var tarefa = new Tarefa() { Id = 2, Nome = "AdicionarFeature", Descricao = "Adicionar feature visualizar tarefas", usuarioResponsavel = "Pedro", IdUsuario = 2, Status = TarefaStatus.EmAndamento };

            _mockTarefaRepositorio.Setup(TarefaRepositorio => TarefaRepositorio.BuscarTarefaPeloId(tarefa.Id)).Returns(tarefa);
            _mockTarefaRepositorio.Setup(TarefaRepositorio => TarefaRepositorio.ConcluirTarefa(tarefa.Id));
            _tarefaNegocio = new TarefaNegocio(_mockUsuarioNegocio.Object,
                                                 _mockUsuarioRepositorio.Object,
                                                _mockTarefaRepositorio.Object);

            try
            {
                _tarefaNegocio.CancelarTarefa(tarefa.Id);
            }
            finally
            {
                Assert.IsTrue(true);
            }
        }
        [TestMethod]
        public void TesteCancelarTarefaQueNaoExiste()
        {

            var tarefaId = 1;

            _mockTarefaRepositorio.Setup(TarefaRepositorio => TarefaRepositorio.BuscarTarefaPeloId(tarefaId)).Returns((Tarefa)null);

            _tarefaNegocio = new TarefaNegocio(_mockUsuarioNegocio.Object,
                                                 _mockUsuarioRepositorio.Object,
                                                _mockTarefaRepositorio.Object);

            var ex = Assert.ThrowsException<Exception>(() => _tarefaNegocio.CancelarTarefa(tarefaId));
            Assert.AreEqual("Tarefa nao existe", ex.Message);
        }
        [TestMethod]
        public void TesteCancelarTarefaStatusInvalido()
        {

            var tarefa = new Tarefa() { Id = 2, Nome = "AdicionarFeature", Descricao = "Adicionar feature visualizar tarefas", usuarioResponsavel = "Pedro", IdUsuario = 2, Status = TarefaStatus.Cancelada };

            _mockTarefaRepositorio.Setup(TarefaRepositorio => TarefaRepositorio.BuscarTarefaPeloId(tarefa.Id)).Returns(tarefa);

            _tarefaNegocio = new TarefaNegocio(_mockUsuarioNegocio.Object,
                                                 _mockUsuarioRepositorio.Object,
                                                _mockTarefaRepositorio.Object);

            var ex = Assert.ThrowsException<Exception>(() => _tarefaNegocio.ConcluirTarefa(tarefa.Id));
            Assert.AreEqual("Tarefa ja foi cancelada ou concluída", ex.Message);
        }
    }
}
