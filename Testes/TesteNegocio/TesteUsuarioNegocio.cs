
namespace Testes
{

    [TestClass]
    public class TesteUsuarioNegocio
    {
        private IUsuarioNegocio _usuarioNegocio;
        private readonly Moq.Mock<IUsuarioRepositorio> _mockUsuarioRepositorio;

        public TesteUsuarioNegocio()
        {
            _mockUsuarioRepositorio = new Moq.Mock<IUsuarioRepositorio>();
        }

        [TestMethod]
        public void TesteCadastrarUsuario()
        {
            var usuario = new Usuario
            {
                Nome = "Eduardo",
                Id = 1,
                userAtual = false
            };
            _mockUsuarioRepositorio.Setup(UsuarioRepositorio => UsuarioRepositorio.BuscarUsuarioPorId(usuario.Id)).Returns((Usuario)null);
            _mockUsuarioRepositorio.Setup(UsuarioRepositorio => UsuarioRepositorio.CadastrarUsuario(usuario));
            _usuarioNegocio = new UsuarioNegocio(_mockUsuarioRepositorio.Object);
            try {
                _usuarioNegocio.CadastrarUsuario(usuario);
            } finally {
                Assert.IsTrue(true);
            }
        }

        [TestMethod]
        public void TesteCadastrarUsuarioVazio()
        {
            _mockUsuarioRepositorio.Setup(UsuarioRepositorio => UsuarioRepositorio.CadastrarUsuario(null));
            _usuarioNegocio = new UsuarioNegocio(_mockUsuarioRepositorio.Object);

            var ex = Assert.ThrowsException<Exception>(() => _usuarioNegocio.CadastrarUsuario(null));
            Assert.AreEqual("Objeto usuario vazio", ex.Message);
        }

        [TestMethod]
        public void TesteCadastrarUsuarioSemId()
        {
            var usuario = new Usuario
            {
                Nome = "Eduardo",
                userAtual = false
            };
            _mockUsuarioRepositorio.Setup(UsuarioRepositorio => UsuarioRepositorio.CadastrarUsuario(usuario));
            _usuarioNegocio = new UsuarioNegocio(_mockUsuarioRepositorio.Object);

            var ex = Assert.ThrowsException<Exception>(() => _usuarioNegocio.CadastrarUsuario(usuario), "Id invalido");
            Assert.AreEqual("Id invalido", ex.Message);
        }
        [TestMethod]
        public void TesteCadastrarUsuarioIdJaExiste()
        {
            var usuario = new Usuario
            {
                Nome = "Eduardo",
                Id = 1,
                userAtual = false
            };
            _mockUsuarioRepositorio.Setup(UsuarioRepositorio => UsuarioRepositorio.BuscarUsuarioPorId(usuario.Id)).Returns(new Usuario { Id = usuario.Id });
            _mockUsuarioRepositorio.Setup(UsuarioRepositorio => UsuarioRepositorio.CadastrarUsuario(usuario));
            _usuarioNegocio = new UsuarioNegocio(_mockUsuarioRepositorio.Object);

            var ex = Assert.ThrowsException<Exception>(() => _usuarioNegocio.CadastrarUsuario(usuario));
            Assert.AreEqual("Id invalido", ex.Message);
        }
        [TestMethod]
        public void TesteCadastrarUsuarioNomeNull()
        {
            var usuario = new Usuario
            {
                Nome = null,
                Id = 5,
                userAtual = false
            };
            _mockUsuarioRepositorio.Setup(UsuarioRepositorio => UsuarioRepositorio.BuscarUsuarioPorId(usuario.Id)).Returns((Usuario)null);
            _mockUsuarioRepositorio.Setup(UsuarioRepositorio => UsuarioRepositorio.CadastrarUsuario(usuario));
            _usuarioNegocio = new UsuarioNegocio(_mockUsuarioRepositorio.Object);

            var ex = Assert.ThrowsException<Exception>(() => _usuarioNegocio.CadastrarUsuario(usuario));
            Assert.AreEqual("Nome invalido", ex.Message);
        }
        [TestMethod]
        public void TesteCadastrarUsuarioNomeTamanhoMenorQue2()
        {
            var usuario = new Usuario
            {
                Nome = "A",
                Id = 5,
                userAtual = false
            };
            _mockUsuarioRepositorio.Setup(UsuarioRepositorio => UsuarioRepositorio.BuscarUsuarioPorId(usuario.Id)).Returns((Usuario)null);
            _mockUsuarioRepositorio.Setup(UsuarioRepositorio => UsuarioRepositorio.CadastrarUsuario(usuario));
            _usuarioNegocio = new UsuarioNegocio(_mockUsuarioRepositorio.Object);

            var ex = Assert.ThrowsException<Exception>(() => _usuarioNegocio.CadastrarUsuario(usuario));
            Assert.AreEqual("Nome invalido", ex.Message);
        }
        [TestMethod]
        public void TesteLoginUsuario()
        {
            var usuario = new Usuario
            {
                Nome = "Eduardo",
                Id = 5,
                userAtual = false
            };
            _mockUsuarioRepositorio.Setup(UsuarioRepositorio => UsuarioRepositorio.BuscarUsuarioPorId(usuario.Id)).Returns(usuario);
            _mockUsuarioRepositorio.Setup(UsuarioRepositorio => UsuarioRepositorio.Login(usuario.Id));
            _usuarioNegocio = new UsuarioNegocio(_mockUsuarioRepositorio.Object);
            try {
                _usuarioNegocio.Login(usuario.Id);
            } finally {
                Assert.IsTrue(true);
            }
            
        }
        [TestMethod]
        public void TesteLoginUsuarioNaoExiste()
        {
            var usuario = new Usuario
            {
                Nome = "Eduardo",
                Id = 5,
                userAtual = false
            };
            _mockUsuarioRepositorio.Setup(UsuarioRepositorio => UsuarioRepositorio.BuscarUsuarioPorId(usuario.Id)).Returns((Usuario)null);
            _mockUsuarioRepositorio.Setup(UsuarioRepositorio => UsuarioRepositorio.Login(usuario.Id));
            _usuarioNegocio = new UsuarioNegocio(_mockUsuarioRepositorio.Object);

            var ex = Assert.ThrowsException<Exception>(() => _usuarioNegocio.Login(usuario.Id));
            Assert.AreEqual("Usuario nao existe", ex.Message);
        }
         [TestMethod]
        public void TesteLoginUsuarioAtual()
        {
            var usuarioId = 5;
            _mockUsuarioRepositorio.Setup(UsuarioRepositorio => UsuarioRepositorio.BuscarUsuarioPorId(usuarioId)).Returns(new Usuario {userAtual = true});
            _mockUsuarioRepositorio.Setup(UsuarioRepositorio => UsuarioRepositorio.Login(usuarioId));
            _usuarioNegocio = new UsuarioNegocio(_mockUsuarioRepositorio.Object);

            var ex = Assert.ThrowsException<Exception>(() => _usuarioNegocio.Login(usuarioId));
            Assert.AreEqual("Usuario ja esta logado", ex.Message);
        }
    }
}
