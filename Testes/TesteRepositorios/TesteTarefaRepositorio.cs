
namespace Testes
{

    [TestClass]
    public class TesteTarefaRepositorio
    {
        private readonly ITarefaRepositorio _TarefaRepositorio;

        public TesteTarefaRepositorio()
        {
            var ServiceCollection = new ServiceCollection();
            ServiceCollection.AddSingleton<ITarefaRepositorio, TarefaRepositorio>();
            var serviceProvider = ServiceCollection.BuildServiceProvider();
            _TarefaRepositorio = serviceProvider.GetService<ITarefaRepositorio>();
        }

        [TestMethod]
        public void TesteCadastrarTarefa()
        {
            Tarefa novaTarefa = new Tarefa { Id = 5, Nome = "testarRepositorio", Descricao = "Testar classe tarefa repositorio", usuarioResponsavel = "Pedro", Status = TarefaStatus.EmAndamento };
            _TarefaRepositorio.CadastrarTarefa(novaTarefa);
            Assert.AreEqual(novaTarefa, _TarefaRepositorio.BuscarTarefaPeloId(5));
        }

        [TestMethod]
        public void TesteBuscarTarefasPeloIdUsuarioQuePossuiTarefasAtribuidas()
        {
            int idUsuario = 1;
            var tarefasUsuario = _TarefaRepositorio.BuscarTarefasPeloResponsavel(idUsuario);
            Assert.AreEqual(2, tarefasUsuario.Count());
        }
        [TestMethod]
        public void TesteBuscarTarefasPeloIdUsuarioQueNaoPossuiTarefasAtribuidas()
        {
            var idUsuario = 4;
            var tarefasUsuario = _TarefaRepositorio.BuscarTarefasPeloResponsavel(idUsuario);
            Assert.IsFalse(tarefasUsuario.Any());
        }
        [TestMethod]
        public void TesteBuscarTarefaQueExistePeloId()
        {
            var tarefaId = 1;
            var tarefa = _TarefaRepositorio.BuscarTarefaPeloId(tarefaId);
            Assert.AreEqual(tarefaId, tarefa.Id);
        }
        [TestMethod]
        public void TesteBuscarTarefaQueNaoExistePeloId()
        {
            var tarefaId = 5;
            var tarefa = _TarefaRepositorio.BuscarTarefaPeloId(tarefaId);
            Assert.IsNull(tarefa);
        }
        [TestMethod]
        public void TesteMudarResponsavelPelaTarefa()
        {
            Usuario novoUsuario = new Usuario
            {
                Nome = "Joao",
                Id = 5
            };
            int tarefaId = 1;
            _TarefaRepositorio.MudarResponsavelPelaTarefa(tarefaId, novoUsuario);
            var tarefa = _TarefaRepositorio.BuscarTarefaPeloId(tarefaId);
            Assert.AreEqual(novoUsuario.Nome, tarefa.usuarioResponsavel);
            Assert.AreEqual(novoUsuario.Id, tarefa.IdUsuario);
        }
        [TestMethod]
        public void TesteConcluirTarefa()
        {

            int tarefaId = 3;
            var tarefa = _TarefaRepositorio.BuscarTarefaPeloId(tarefaId);
            Assert.AreEqual(TarefaStatus.EmAndamento, tarefa.Status);
            _TarefaRepositorio.ConcluirTarefa(tarefaId);
            Assert.AreEqual(TarefaStatus.Concluida, tarefa.Status);

        }
        [TestMethod]
        public void TesteCancelarTarefa()
        {

            int tarefaId = 3;
            var tarefa = _TarefaRepositorio.BuscarTarefaPeloId(tarefaId);
            Assert.AreEqual(TarefaStatus.EmAndamento, tarefa.Status);
            _TarefaRepositorio.CancelarTarefa(tarefaId);
            Assert.AreEqual(TarefaStatus.Cancelada, tarefa.Status);

        }
    }
}