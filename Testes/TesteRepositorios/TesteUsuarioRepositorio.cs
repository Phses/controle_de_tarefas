namespace Testes
{
    [TestClass]
    public class TesteUsuarioRepositorio
    {
        private readonly IUsuarioRepositorio _usuarioRepositorio;

        public TesteUsuarioRepositorio()
        {
            var ServiceCollection = new ServiceCollection();
            ServiceCollection.AddSingleton<IUsuarioRepositorio, UsuarioRepositorio>();
            var serviceProvider = ServiceCollection.BuildServiceProvider();
            _usuarioRepositorio = serviceProvider.GetService<IUsuarioRepositorio>();
        }

        [TestMethod]
        public void TesteBuscarUsuarioQueExistePorId()
        {
            var usuarioId = 1;
            var usuario = _usuarioRepositorio.BuscarUsuarioPorId(usuarioId);
            Assert.AreEqual(usuarioId, usuario.Id);
        }

        [TestMethod]
        public void TesteBuscarUsuarioQueNaoExistePorId()
        {
            var usuarioId = 5;
            var usuario = _usuarioRepositorio.BuscarUsuarioPorId(usuarioId);
            Assert.IsNull(usuario);
        }
        [TestMethod]
        public void TesteBuscarIdUsuarioLogado()
        {
            var usuarioId = _usuarioRepositorio.BuscarIdUsuarioAtual();
            Assert.IsTrue(_usuarioRepositorio.BuscarUsuarioPorId(usuarioId).userAtual);
        }
        [TestMethod]
        public void TesteCadastrarUsuario()
        {
            var usuario = new Usuario { Nome = "Jose", Id = 5, userAtual = false };
            _usuarioRepositorio.CadastrarUsuario(usuario);
            Assert.AreEqual(usuario, _usuarioRepositorio.BuscarUsuarioPorId(usuario.Id));
        }
        [TestMethod]
        public void TesteLogin()
        {
            var usuarioId = 2; 
            _usuarioRepositorio.Login(usuarioId);
            Assert.IsTrue(_usuarioRepositorio.BuscarUsuarioPorId(usuarioId).userAtual);
        }
    }
}