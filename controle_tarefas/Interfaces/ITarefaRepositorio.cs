using controle_tarefas.Model;

namespace controle_tarefas.Intefaces {
    public interface ITarefaRepositorio {
        public void CadastrarTarefa(Tarefa tarefa);
        public List<Tarefa> BuscarTarefasPeloResponsavel(int idUsuario);
        public Tarefa BuscarTarefaPeloId (int id);
        public void MudarResponsavelPelaTarefa(int tarefaId, Usuario usuario);
        public void ConcluirTarefa(int tarefaId);
        public void CancelarTarefa(int tarefaId);
    }
}