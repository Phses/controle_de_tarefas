using controle_tarefas.Model;
namespace controle_tarefas.Intefaces
{
    public interface ITarefaNegocio
    {
        public void VisualizarMinhasTarefas();
        public void VisualizarTarefasUsuario(int usuarioId);
        public void MudarResponsavelPelaTarefa(int tarefaId, Usuario usuario);
        public void CadastrarTarefa(Tarefa tarefa);
        public void ConcluirTarefa(int tarefaId);
        public void CancelarTarefa(int tarefaId);
    }
}