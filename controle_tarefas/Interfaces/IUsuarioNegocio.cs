using controle_tarefas.Model;

namespace controle_tarefas.Intefaces
{
    public interface IUsuarioNegocio
    {
        public void CadastrarUsuario(Usuario usuario);
        public void Login(int usuarioId);
    }
}