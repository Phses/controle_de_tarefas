using controle_tarefas.Model;

namespace controle_tarefas.Intefaces {
    public interface IUsuarioRepositorio {
        public Usuario BuscarUsuarioPorId(int usuarioId);
        public int BuscarIdUsuarioAtual();
        public void CadastrarUsuario(Usuario usuario);
        public void Login(int usuarioId);
    }
}