using controle_tarefas.Intefaces;
using controle_tarefas.Model;

namespace controle_tarefas.Negocio {
    public class UsuarioNegocio : IUsuarioNegocio {
        private readonly IUsuarioRepositorio _usuarioRespositorio;

        public UsuarioNegocio(IUsuarioRepositorio usuarioRepositorio) {
            _usuarioRespositorio = usuarioRepositorio;
        }

        public void CadastrarUsuario(Usuario usuario) {
            if(usuario == null) {
                throw new Exception("Objeto usuario vazio");
            }
            if(usuario.Id == 0 || _usuarioRespositorio.BuscarUsuarioPorId(usuario.Id) != null) {
                throw new Exception("Id invalido");
            }
            if(usuario.Nome == null || usuario.Nome.Length < 2) {
                throw new Exception("Nome invalido");
            }
            _usuarioRespositorio.CadastrarUsuario(usuario);
        }

        public void Login(int usuarioId) {
            Console.WriteLine("Metodo Login");
            if(_usuarioRespositorio.BuscarUsuarioPorId(usuarioId) == null) {
                throw new Exception("Usuario nao existe");
            }
            if(_usuarioRespositorio.BuscarUsuarioPorId(usuarioId).userAtual == true) {
                throw new Exception("Usuario ja esta logado");
            }
            _usuarioRespositorio.Login(usuarioId);
            Console.WriteLine("Usuario atual alterado");
        }

    }
}