using controle_tarefas.Model;
using controle_tarefas.Intefaces;
using controle_tarefas.Enums;

namespace controle_tarefas.Negocio {
    public class TarefaNegocio : ITarefaNegocio {
        private readonly IUsuarioNegocio _usuarioNegocio;
        private readonly IUsuarioRepositorio _usuarioRepositorio;
        private readonly ITarefaRepositorio _tarefaRepositorio;
        
        public TarefaNegocio(IUsuarioNegocio usuarioNegocio, IUsuarioRepositorio usuarioRepositorio, ITarefaRepositorio tarefaRepositorio) {
            _usuarioNegocio = usuarioNegocio;
            _usuarioRepositorio = usuarioRepositorio;
            _tarefaRepositorio = tarefaRepositorio;
        }

        public void VisualizarMinhasTarefas(){
            int usuarioId = _usuarioRepositorio.BuscarIdUsuarioAtual();
            Console.WriteLine("Metodo visualizar minhas tarefas");
            Console.WriteLine("Id Usuario atual = " + usuarioId);
            Console.WriteLine();
            List<Tarefa> minhasTarefas = _tarefaRepositorio.BuscarTarefasPeloResponsavel(usuarioId);
            if(!minhasTarefas.Any()) {
                throw new Exception("Nao ha tarefas a serem mostradas");
            }
            Console.WriteLine("Minhas tarefas:");
            foreach(var tarefa in minhasTarefas) {
                Console.WriteLine(tarefa);
                Console.WriteLine();
            }
        }

        public void VisualizarTarefasUsuario(int usuarioId) {
            Console.WriteLine("Metodo visualizar tarefas outros usuarios");
            Console.WriteLine();
            List<Tarefa> tarefas = _tarefaRepositorio.BuscarTarefasPeloResponsavel(usuarioId);
            var usuario = _usuarioRepositorio.BuscarUsuarioPorId(usuarioId);
            if(!tarefas.Any()) {
                throw new Exception("Nao ha tarefas a serem mostradas");
            }
            Console.WriteLine("Tarefas " + usuario.Nome + " :");
            foreach(var tarefa in tarefas) {
                Console.WriteLine(tarefa);
                Console.WriteLine();
            }
        }

        public void MudarResponsavelPelaTarefa(int tarefaId, Usuario usuario){
            Console.WriteLine("Metodo mudar responsavel pela tarefa");
             var tarefa = _tarefaRepositorio.BuscarTarefaPeloId(tarefaId);

            if(tarefa == null) {
                throw new Exception("Tarefa nao existe");
            } 
            
            if(_usuarioRepositorio.BuscarUsuarioPorId(usuario.Id) == null) {
                Console.WriteLine("Usuario nao existe");
                _usuarioNegocio.CadastrarUsuario(usuario);
                Console.WriteLine("Usuario cadastrado");
                Console.WriteLine("Usuario responsavel " + tarefa.usuarioResponsavel);
                _tarefaRepositorio.MudarResponsavelPelaTarefa(tarefaId, usuario);
                Console.WriteLine("Reponsavel alterado com sucesso");
                Console.WriteLine("Usuario responsavel atual " + tarefa.usuarioResponsavel);
                Console.WriteLine(tarefa);
            } else {
                Console.WriteLine("Usuario responsavel " + tarefa.usuarioResponsavel);
                _tarefaRepositorio.MudarResponsavelPelaTarefa(tarefaId, usuario);
                Console.WriteLine("Usuario responsavel atual " + tarefa.usuarioResponsavel);
                Console.WriteLine("Reponsavel alterado com sucesso");
                Console.WriteLine(tarefa);
            }
        }
        public void CadastrarTarefa(Tarefa tarefa) {
            Console.WriteLine("Metodo cadastrar tarefa");
            if(tarefa.Nome == null || tarefa.Nome.Length < 2) {
                throw new Exception("Nome inválido");
            }
            if(tarefa.Id == 0) {
                throw new Exception("Id invalido");
            }
            if(_tarefaRepositorio.BuscarTarefaPeloId(tarefa.Id) != null) {
                throw new Exception("Tarefa ja existe");
            }
            if(tarefa.Descricao == null || tarefa.Descricao.Length < 2) {
                throw new Exception("Descricao invalida");
            }
            if(_usuarioRepositorio.BuscarUsuarioPorId(tarefa.IdUsuario) == null) {
                throw new Exception("Usuario responsavel nao cadastrado");
            }
            
            _tarefaRepositorio.CadastrarTarefa(tarefa);
            Console.WriteLine("Tarefa cadastrada com sucesso");
        }

        public void ConcluirTarefa(int tarefaId) {
            Console.WriteLine("Metodo concluir tarefa");
            var tarefa = _tarefaRepositorio.BuscarTarefaPeloId(tarefaId);
            if(tarefa == null) {
                throw new Exception("Tarefa nao existe");
            }
            if(tarefa.Status != TarefaStatus.EmAndamento) {
                throw new Exception("Tarefa ja foi cancelada ou concluída");
            }
            _tarefaRepositorio.ConcluirTarefa(tarefaId);
            Console.WriteLine("Status da tarefa alterado para concluido");
            Console.WriteLine(tarefa);
        }
        public void CancelarTarefa(int tarefaId) {
            Console.WriteLine("Metodo cancelar tarefa");
            var tarefa = _tarefaRepositorio.BuscarTarefaPeloId(tarefaId);
            if(tarefa == null) {
                throw new Exception("Tarefa nao existe");
            }
            if(tarefa.Status != TarefaStatus.EmAndamento) {
                throw new Exception("Tarefa ja foi cancelada ou concluída");
            }
            _tarefaRepositorio.CancelarTarefa(tarefaId);
            Console.WriteLine("Status da tarefa alterado para cancelada");
            Console.WriteLine(tarefa);
        }
    }
}