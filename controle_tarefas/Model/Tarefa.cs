using controle_tarefas.Enums;

namespace controle_tarefas.Model
{
    public class Tarefa
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string usuarioResponsavel { get; set; }
        public int IdUsuario { get; set; }
        public TarefaStatus Status { get; set; }

        public override string ToString()
        {
            return "Descricao da tarefa |  " +
                    Descricao +
                    "  |" +
                    "\r\n" +
                    "Usuario responsavel |  " +
                    usuarioResponsavel +
                    "  |" +
                    "\r\n" +
                    "Status da tarefa    |  " +
                    Status +
                    "  |";
        }
    }
}