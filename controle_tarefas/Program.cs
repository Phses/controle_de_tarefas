﻿using Microsoft.Extensions.DependencyInjection;
using controle_tarefas.Model;
using controle_tarefas.Intefaces;
using controle_tarefas.Repositorio;
using controle_tarefas.Negocio;
using controle_tarefas.Enums;

public class Program {
    static void Main(string[] args) {
        var serviceCollection = new ServiceCollection();
        serviceCollection.AddSingleton<IUsuarioRepositorio, UsuarioRepositorio>();
        serviceCollection.AddSingleton<IUsuarioNegocio, UsuarioNegocio>();
        serviceCollection.AddSingleton<ITarefaRepositorio, TarefaRepositorio>();
        serviceCollection.AddSingleton<ITarefaNegocio, TarefaNegocio>();

        var serviceProvider = serviceCollection.BuildServiceProvider();

        var tarefaNegocio = serviceProvider.GetService<ITarefaNegocio>();
        var usuarioNegocio = serviceProvider.GetService<IUsuarioNegocio>();

        //visualizar tarefas do usuario atual
        tarefaNegocio.VisualizarMinhasTarefas();

        Console.WriteLine();

        //Visualizar tarefas do usuario de Id = 2
        int usuarioId = 2;
        tarefaNegocio.VisualizarTarefasUsuario(usuarioId);

        Console.WriteLine();

        //Mudar usuario atual e visualizar suas tarefas
        usuarioNegocio.Login(usuarioId);
        tarefaNegocio.VisualizarMinhasTarefas();

        Console.WriteLine();

        //Mudar o responsavel por uma tarefa 

        var novoUsuario = new Usuario {
            Nome = "Dom",
            Id = 5,
            userAtual = false
        };

        int tarefaId = 2;
        tarefaNegocio.MudarResponsavelPelaTarefa(tarefaId, novoUsuario);

        Console.WriteLine();

        //Cadastrar uma tarefa 

        var novaTarefa = new Tarefa {
            Id = 5,
            Nome = "EnviarExercicio",
            Descricao = "Enviar exercicio 03 injecao de dependencia",
            usuarioResponsavel = novoUsuario.Nome,
            IdUsuario = novoUsuario.Id,
            Status = TarefaStatus.EmAndamento
        };

        tarefaNegocio.CadastrarTarefa(novaTarefa);

        Console.WriteLine();

        //Concluir uma tarefa
        var idTarefa = 2;
        tarefaNegocio.ConcluirTarefa(idTarefa);

        Console.WriteLine();

        //Cancelar uma tarefa
        var idTarefa2 = 3;
        tarefaNegocio.CancelarTarefa(idTarefa2);

        Console.WriteLine();

        Console.WriteLine("Fim do programa");
    }
}