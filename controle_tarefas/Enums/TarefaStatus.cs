namespace controle_tarefas.Enums {
    public enum TarefaStatus {
        EmAndamento,
        Concluida,
        Cancelada,
    }
}