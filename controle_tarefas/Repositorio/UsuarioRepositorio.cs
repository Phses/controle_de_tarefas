using controle_tarefas.Model;
using controle_tarefas.Intefaces;

namespace controle_tarefas.Repositorio
{
    public class UsuarioRepositorio : IUsuarioRepositorio
    {
        public List<Usuario> Usuarios = new List<Usuario>();

        public UsuarioRepositorio()
        {
            Usuarios.Add(new Usuario { Nome = "Pedro", Id = 1, userAtual = true });
            Usuarios.Add(new Usuario { Nome = "Cecilia", Id = 2, userAtual = false });
            Usuarios.Add(new Usuario { Nome = "Camila", Id = 3, userAtual = false });
            Usuarios.Add(new Usuario { Nome = "Joao", Id = 4, userAtual = false });
        }

        public Usuario BuscarUsuarioPorId(int usuarioId)
        {
            return Usuarios.Where(usuario => usuario.Id == usuarioId).FirstOrDefault();
        }

        public int BuscarIdUsuarioAtual() {
            var usuario = Usuarios.Where(usuario => usuario.userAtual == true).FirstOrDefault();
            return usuario.Id;
        }

        public void CadastrarUsuario(Usuario usuario)
        {
            Usuarios.Add(usuario);
        }

        public void Login(int usuarioId)
        {
            var usuarioAtual = Usuarios.Where(usuario => usuario.userAtual = true).FirstOrDefault();
            usuarioAtual.userAtual = false;
            var usuarioNovoLogin = BuscarUsuarioPorId(usuarioId);
            usuarioNovoLogin.userAtual = true;
        }
    }
}