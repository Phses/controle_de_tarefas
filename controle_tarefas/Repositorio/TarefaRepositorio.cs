using controle_tarefas.Model;
using controle_tarefas.Enums;
using controle_tarefas.Intefaces;

namespace controle_tarefas.Repositorio {
    public class TarefaRepositorio : ITarefaRepositorio {
        public List<Tarefa> Tarefas = new List<Tarefa>();

        public TarefaRepositorio() {
            Tarefas.Add(new Tarefa {Id = 1, Nome = "PagarConta", Descricao = "Pagar conta de água", usuarioResponsavel = "Cecilia", IdUsuario = 2, Status = TarefaStatus.Cancelada});
            Tarefas.Add(new Tarefa {Id = 2, Nome = "AdicionarFeature", Descricao = "Adicionar feature visualizar tarefas", usuarioResponsavel = "Pedro", IdUsuario = 1, Status = TarefaStatus.EmAndamento});
            Tarefas.Add(new Tarefa {Id = 3, Nome = "TestarNegocio", Descricao = "Testar classe negocio", usuarioResponsavel = "Pedro", IdUsuario = 1, Status = TarefaStatus.EmAndamento});
            Tarefas.Add(new Tarefa {Id = 4, Nome = "CriarRepositorio", Descricao = "Criar repositorio no Git Lab do atual projeto", usuarioResponsavel = "Camila", IdUsuario = 3, Status = TarefaStatus.Concluida});
        }

        public void CadastrarTarefa(Tarefa tarefa) {
            Tarefas.Add(tarefa);
        } 

        public List<Tarefa> BuscarTarefasPeloResponsavel (int idUsuario) {
            return Tarefas.Where(tarefa => tarefa.IdUsuario == idUsuario).ToList();
        }

        public Tarefa BuscarTarefaPeloId (int id) {
            return Tarefas.Where(tarefa => tarefa.Id == id).FirstOrDefault();
        }

        public void MudarResponsavelPelaTarefa(int tarefaId, Usuario usuario) {
            // Tarefas.Where(tarefa => tarefa.Id == tarefaId).Select(tarefa => tarefa.usuarioResponsavel = novoResponsavel);
            var tarefa = BuscarTarefaPeloId(tarefaId);
            tarefa.usuarioResponsavel = usuario.Nome;
            tarefa.IdUsuario = usuario.Id;
        }

        public void ConcluirTarefa(int tarefaId) {
            var tarefa  = BuscarTarefaPeloId(tarefaId);
            tarefa.Status = TarefaStatus.Concluida;
        }
        public void CancelarTarefa(int tarefaId) {
            var tarefa  = BuscarTarefaPeloId(tarefaId);
            tarefa.Status = TarefaStatus.Cancelada;
        }
    }
}